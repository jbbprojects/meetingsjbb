/* ==================================================================== */
/* This module contains helpers for building of Scilab gateways.        */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
// 

// Note: inout parameter 'success' is modified only from true to false.
// If on enter the parameter 'success' is 0, the function returns 0 or NULL
// immediately.

double getDouble  (int index, double minValue, double maxValue, int *success);

int    getInt     (int index, int minValue,    int maxValue,    int *success);

void * getPtr     (int index, int *success);

int    getBoolean (int index, int *success);

char * getString  (int index, int minLength,   int maxLength,   int* len,  int *success);
// The string must be deallocated by caller. Parameter 'len' may be NULL.
// Answer must be deallocated by caller if success.

char * getBytes   (int index, int minLength,   int maxLength,   int* len,  int* freeIt, int *success);
// Both string scalars and int8 vectors are accepted.
// The answer must be deallocated by caller if success && freeIt. 
// Parameter 'len' may be NULL.

double* getDoubleVector (int index, int minLength, int maxLength, int* len, int* success);
// Vectors of double are accepted. Don't deallocate the answer!!!
// Parameter 'len' may be NULL.

// Planned but not implemented:
// int*    getIntVector    (int index, int minLength, int maxLength, int* len, int* success);
// char**  getStringVector (int index, int minLength, int maxLength, int* len, int* success);

void returnDouble (int index, double value);

void returnInt    (int index, int    value);

void returnPtr    (int index, void * value);

void returnString (int index, char *value);

void returnBytes  (int index, char *value, int len);
