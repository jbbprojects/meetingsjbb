/* ==================================================================== */
/* Serial port - low level bindings */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/ioctl.h>
/* ==================================================================== */
int serialread(char *fname)
{
    int success = 1;
    struct termios params;
    int status;
    int ans;
    /* check that we have 1..3 input arguments */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx, 1, 3);
    CheckOutputArgument(pvApiCtx, 1, 1);
    
    int fd = getInt( 1, 0, 0x7FFFFFFF, &success );
    if (!success) return 0;

    if (!isatty(fd) || (tcgetattr(fd, &params) == -1))
    {
        Scierror(999, "It is not a serial port.\n");
        return 0;
    }   
    
    int maxlen = 1024;
    int as_bytes = 0;
    
    if (nbInputArgument(pvApiCtx) > 1)
    {
        maxlen = getInt( 2, 1, 0x7FFF, &success );
    }

    if (nbInputArgument(pvApiCtx) > 2)
    {   
        as_bytes = getBoolean( 3, &success );
    }
    
    if (!success) return 0;

    //-----------------------------------------------------------------
    char * buffer = (char*)malloc(sizeof(char) * (maxlen+1));
    int actual = read(fd, buffer, maxlen);
    if ((actual < 0) || (actual > maxlen)) actual = 0;
    buffer[actual] = 0x00;

    //-----------------------------------------------------------------

    /* create result on stack */
    if (!as_bytes)
    {
        returnString(1, buffer);
    }
    else
    {
        returnBytes(1, buffer, actual);
    }
    free(buffer);
    ReturnArguments(pvApiCtx);
    return 0;
}
