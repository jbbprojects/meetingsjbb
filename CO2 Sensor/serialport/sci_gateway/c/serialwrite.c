/* ==================================================================== */
/* Serial port - low level bindings */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/ioctl.h>
/* ==================================================================== */
int serialwrite(char *fname)
{
    /* check that we have 2 input arguments */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx, 2, 2);
    CheckOutputArgument(pvApiCtx, 1, 1);

    int success = 1;
    int fd = getInt( 1, 0, 0x7FFFFFFF, &success );
    int len;
    int freeIt;
    
    char *data = getBytes( 2, 0, 0x7FFF, &len,  &freeIt, &success ); 
    if (!success) return 0;
    
    int ans = write(fd, data, len);

    if (freeIt) free(data);

    /* create result on stack */
    returnInt( 1, ans );
    ReturnArguments(pvApiCtx);
    return 0;
}
/* ==================================================================== */

