/* ==================================================================== */
/* Serial port - low level bindings */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/ioctl.h>
#define  HARD 1
#define  SOFT 2
/* ==================================================================== */
int serialset(char *fname)
{
    int success = 1;
    struct termios params;
    int status;
    int ans;
    
    /* check that we have 3 input arguments */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx, 3, 3);
    CheckOutputArgument(pvApiCtx, 1, 1);
    
    int fd = getInt( 1, 0, 0x7FFFFFFF, &success );
    if (!success) return 0;

    if (!isatty(fd) || (tcgetattr(fd, &params) == -1))
    {
      Scierror(999, "It is not a serial port.\n");
      return 0;
    }   
    
    char *field = getString( 2, 1, 12, NULL, &success ); 
    if (!success) return 0;

    int value = getInt( 3, 0, 0x7FFFFFFF, &success );
    if (!success) 
    {
      free(field);
      return 0;
    }
    
    int baud;
    int bits;

    //-----------------------------------------------------------------
    
    if (strcmp(field, "baud") == 0)
    {
      switch (value)
      {
        case      50: baud =      B50; break;  
        case      75: baud =      B75; break;  
        case     110: baud =     B110; break;  
        case     134: baud =     B134; break;  
        case     150: baud =     B150; break;  
        case     200: baud =     B200; break;  
        case     300: baud =     B300; break;  
        case     600: baud =     B600; break;  
        case    1200: baud =    B1200; break;  
        case    2400: baud =    B2400; break;  
        case    4800: baud =    B4800; break;  
        case    9600: baud =    B9600; break;  
        case   19200: baud =   B19200; break;  
        case   38400: baud =   B38400; break;  
        case   57600: baud =   B57600; break;
#ifdef B76800
        case   76800: baud =   B76800; break;
#endif
#ifdef B115200
        case  115200: baud =  B115200; break;  
#endif
#ifdef B230400
        case  230400: baud =  B230400; break;  
#endif
#ifdef B460800
        case  460800: baud =  B460800; break;  
#endif
#ifdef B500000
        case  500000: baud =  B500000; break;  
#endif
#ifdef B576000
        case  576000: baud =  B576000; break;  
#endif
#ifdef B921600
        case  921600: baud =  B921600; break;  
#endif
#ifdef B1000000
        case 1000000: baud = B1000000; break;
#endif
        default:
          Scierror(999, "%s: Wrong value for input argument #%d:\n", fname, 3);
      }
      cfsetispeed(&params, baud);
      cfsetospeed(&params, baud);
      ans = tcsetattr(fd, TCSANOW, &params);
    }
    else if (strcmp(field, "data_bits") == 0)
    {
      switch (value)
      {
        case      5: bits =      CS5; break;  
        case      6: bits =      CS6; break;  
        case      7: bits =      CS7; break;  
        case      8: bits =      CS8; break;  
        default:
          Scierror(999, "%s: Wrong value for input argument #%d:\n", fname, 3);
      }
      params.c_cflag &= ~CSIZE;
      params.c_cflag  |= bits;
      ans = tcsetattr(fd, TCSANOW, &params);
    }
    else if (strcmp(field, "stop_bits") == 0)
    {
      switch (value)
      {
        case      1: bits =      0; break;  
        case      2: bits = CSTOPB; break;  
        default:
          Scierror(999, "%s: Wrong value for input argument #%d:\n", fname, 3);
      }
      params.c_cflag &= ~CSTOPB;
      params.c_cflag  |= bits;
      ans = tcsetattr(fd, TCSANOW, &params);
    }
    else if (strcmp(field, "parity") == 0)
    {
      switch (value)
      {
        case 2:
          params.c_cflag |= PARENB;
          params.c_cflag &= ~PARODD;
          break;
        case 1:
          params.c_cflag |= PARENB;
          params.c_cflag |= PARODD;
          break;
        default:
          params.c_cflag &= ~PARENB;
      }
      ans = tcsetattr(fd, TCSANOW, &params);
    }
    else if (strcmp(field, "break") == 0)
    {
      ans = tcsendbreak(fd, value / 3);
    }
    else if (strcmp(field, "flow_control") == 0)
    {
      if (value & HARD)
        params.c_cflag |= CRTSCTS;
      else  
        params.c_cflag &= ~CRTSCTS;
      if (value & SOFT)
        params.c_iflag |= (IXON | IXOFF | IXANY);
      else  
        params.c_iflag &= ~(IXON | IXOFF | IXANY);
      ans = tcsetattr(fd, TCSANOW, &params);
    }
    else if (strcmp(field, "timeout") == 0)
    {
      if (value < 0)
      {
        params.c_cc[VTIME] = 0;
        params.c_cc[VMIN] = 0;
      }
      else if (value == 0)
      {
        params.c_cc[VTIME] = 0;
        params.c_cc[VMIN] = 1;
      }
      else   
      {
        params.c_cc[VTIME] = (value + 50) / 100;
        params.c_cc[VMIN] = 0;
      }
      ans = tcsetattr(fd, TCSANOW, &params);
    }
    else 
    {
      ioctl(fd, TIOCMGET, &status);
      if (strcmp(field, "dtr") == 0)
      {
        if (value) status |= TIOCM_DTR; else status &= ~TIOCM_DTR; 
      }
      else if (strcmp(field, "dsr") == 0)
      {
        if (value) status |= TIOCM_DSR; else status &= ~TIOCM_DSR; 
      }
      else if (strcmp(field, "rts") == 0)
      {
        if (value) status |= TIOCM_RTS; else status &= ~TIOCM_RTS; 
      }
      else if (strcmp(field, "cts") == 0)
      {
        if (value) status |= TIOCM_CTS; else status &= ~TIOCM_CTS; 
      }
      else if (strcmp(field, "dcd") == 0)
      {
        if (value) status |= TIOCM_CD; else status &= ~TIOCM_CD; 
      }
      else if (strcmp(field, "ri") == 0)
      {
        if (value) status |= TIOCM_RI; else status &= ~TIOCM_RI; 
      }
      else
      {
        Scierror(100, "%s: Unknown attribute %s\n", fname, field);
        ans = -1;
      }  
    }
    
    //-----------------------------------------------------------------
    // Cleanup
    free(field);

    /* create result on stack */
    returnInt( 1, ans );
    ReturnArguments(pvApiCtx);
    return 0;
}
/* ==================================================================== */

