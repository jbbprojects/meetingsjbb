const int LED_PIN=13;
void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  //Prendemos el led
  digitalWrite(LED_PIN, HIGH);
  //Detenemos la ejecución durante 500ms, en los cuales el led permanecerá encendido
  delay(500);

  //Apagamos el LED
  digitalWrite(LED_PIN, LOW);

  //Detenemos las ejecución durante 500ms, en los cuales el led permanecerá apagado
  delay(500);
}
