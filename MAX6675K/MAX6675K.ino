
#define CLK 9
#define DBIT 10 //so
#define CS 13

#include<SoftwareSerial.h>

int v=0;
float Ctemp, Ftemp;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
pinMode(CLK,OUTPUT);
pinMode(DBIT,INPUT);
pinMode(CS,OUTPUT);
digitalWrite(CS,HIGH);
digitalWrite(CLK,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  v=spiRead();
  if(v==-1){
    Serial.print("No sensor \n");
    }
   else{
    Ctemp=v*0.25;
    Ftemp=(Ctemp*9/5)+32;
    
    //Serial.print("\n Temperature in C");
    Serial.print(Ctemp);
    Serial.print("\n");
    //Serial.print("\n Temperature in F");
    //Serial.print(Ftemp);
    
    }
    delay(100);
}

int spiRead(){
  int value = 0;
  digitalWrite(CS,LOW);
  delay(2);
  digitalWrite(CS,HIGH);
  delay(220);
  digitalWrite(CS,LOW);
  digitalWrite(CLK,HIGH);
  delay(1);
  digitalWrite(CLK,LOW);
  for (int i=14;i>=0;i--){
    digitalWrite(CLK,HIGH);
    value+=digitalRead(DBIT)<<i;
    digitalWrite(CLK,LOW);
    }
    if((value & 0x04)==0x04) return -1;
    return value >> 3;
  }
