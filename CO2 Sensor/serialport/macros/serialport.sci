// ====================================================================
//* Serial port - high level object */
//* This file is released under the terms of the CeCILL_V2 license
// ====================================================================
//
//

// Internal: find serial port data by name
//
//   name - name of serial port
//
// Returns index or 0
//
function index = serialport_find( name )
    global serialport_data
    // list of structures
    if serialport_data == [] then
        serialport_data = list();
    end
    
    for i = 1:length(serialport_data)
        if serialport_data(i).name == name then
            index = i;
            return;
        end
    end
    
    index = 0;
endfunction

// Public: create instance of serial port object
//   
//   name     - string, name or number of serial port
//   varargin - strings or numbers, baud rate, parity, data_bits and stop bits
//
// Example:
//   s = serialport('/dev/ttyS4', '19200,N,8,1')
// or
//   s = serialport('/dev/ttyS4', 19200, 'N',8,1)
// or even
//   s = serialport('/dev/ttyS4', 19200, 'N,8,1')
//
// Returns link to serial port object (tlist)
//
function s = serialport(name,varargin)
    if type(name) ~= 10 then
        name = sprintf( '/dev/ttyS%i', name );
    else
        name = stripblanks( name, %t );
    end
    
    param = list();
    for i = 1:argn(2)-1
        arg = varargin(i);
        select type(arg)
        case 1 then      // number
            if length(arg) ~= 1 then
                error( 'Wrong parameter of serialport: Scalar expected' )
            end
            param($+1) = arg;
        case 10 then     // string
            for t = tokens(arg,',')',
                param($+1) = t;
            end
        else
            error( 'Wrong parameter of serialport: String or numeric scalar expected' )
        end
    end
    
    default = list(9600,'N',8,1);
    for i = 1:length(default),
        if i > length(param) then
            param(i) = default(i);
        elseif type(param(i)) == 10 then
            if param(i) == '' then
                param(i) = default(i);
            elseif isnum(param(i)) then
                param(i) = evstr(param(i));
            end
        end            
    end
    
    ind = serialport_find( name );
    global serialport_data;
    if ind == 0 then
        ind = length(serialport_data) + 1;
        serialport_data(ind) = struct( 'name', name, ...
                                       'fd', 0,      ...
                                       'line_separator', ascii(10), ...
                                       'buffer', '');
    end

    if serialport_data(ind).fd > 0 then 
        serialclose(serialport_data(ind).fd);
        serialport_data(ind).buffer = '';
    end

    fd = serialopen(name, param(1), param(2), param(3), param(4));
    if fd <= 0 then
        serialport_data(ind) = null();
        error( 'Problems opening serial port ' + name )
    end
    serialport_data(ind).fd = fd;
    s = tlist(['serialport','name'], name);
endfunction

// Internal : set property
//   field - string, one of 'baud', 'data_bits', 'stop_bits', 'parity', 'break', 
//                    'flow_control', 'dtr', 'dsr', 'rts', 'cts', 'dcd', 'ri',
//                    'timeout'
//           timeout is measured in milliseconds
// 
// Examples:
//   s.baud = 9600

function obj = %s_i_serialpo(field, value, object)
    global serialport_data
    ind = serialport_find( object.name );
    if ind == 0 then
        error( 'This serial port is already closed' )
    end
    
    if field == 'line_separator' then
        serialport_data(ind).line_separator = ascii(value);
    elseif or(field == ['baud', 'data_bits', 'stop_bits', 'parity', 'break', ...
                    'flow_control', 'dtr', 'dsr', 'rts', 'cts', 'dcd', 'ri', ...
                    'timeout']) then
        serialset(serialport_data(ind).fd, field, value);
    else
        error( 'Field ''' + field + ''' not exists' )
    end
    obj = object;    
endfunction

function obj = %c_i_serialpo(field, value, object)
    global serialport_data
    ind = serialport_find( object.name );
    if ind == 0 then
        error( 'This serial port is already closed' )
    end
    
    if field == 'line_separator' then
        serialport_data(ind).line_separator = value;
    else
        error( 'Field ''' + field + ''' not exists or can''t accept string values' )
    end    
    obj = object;    
endfunction

// Internal : get property or methods
//   field - string, one of 'baud', 'data_bits', 'stop_bits', 'parity', 'break', 
//                    'flow_control', 'dtr', 'dsr', 'rts', 'cts', 'dcd', 'ri',
//                    'timeout'
//           timeout is measured in milliseconds
//           Methods are:
//              read(count[,binary]) -- returns string, default count is 1024
//              readln()             -- read up to line_separator, return string
//              write(str)           -- write string (or several strings)
//              writeln(str1[,str2,..]) -- write string(s) and send line_separator
//                                      after each string and each row of string
//              close()              -- close serial port
// Examples:
//   s.baud = 9600

function res = %serialpo_e(field, object)
    function s = serialport_null()
        s = [];
    endfunction
    
    function s = serialport_read(count,binary)
        global serialport_data
        global serialport_ind
        if ~exists('count', 'local') then count = 1024; end
        if ~exists('binary', 'local') then binary = %f; end
        s = serialread(serialport_data(serialport_ind).fd, count, binary);
    endfunction
    
    function s = serialport_readln()
        global serialport_data
        global serialport_ind
        sep = serialport_data(serialport_ind).line_separator;
        while %t
            buf = serialport_data(serialport_ind).buffer;
            p = strindex(buf, sep);
            if p ~= [] then
                p = p(1);
                s = part(buf, 1:p(1)-1);
                serialport_data(serialport_ind).buffer = ...
                       part( buf, p(1)+length(sep):length(buf) );
                return;
            end
            
            r = serialread(serialport_data(serialport_ind).fd, 1024);
            if length(r) == 0 then 
                error "Timeout"; 
                serialport_data(serialport_ind).buffer = '';
            end;
            serialport_data(serialport_ind).buffer = buf + r;
        end
    endfunction
    
    function serialport_write(varargin)
        global serialport_data
        global serialport_ind
        for i = 1:argn(2),
            serialwrite(serialport_data(serialport_ind).fd, varargin(i));
        end
    endfunction
    
    function serialport_writeln(varargin)
        global serialport_data
        global serialport_ind
        fd = serialport_data(serialport_ind).fd;
        for i = 1:argn(2),
            x = varargin(i);
            [r,c] = size(x);
            for i = 1:r,
                for item = x(i,:)
                    serialwrite(fd, item);
                end
                serialwrite(fd, ...
                        serialport_data(serialport_ind).line_separator);
            end
        end
    endfunction
    
    ind = serialport_find( object.name );
    if ind == 0 then
        error( 'This serial port is already closed' )
    end
    
    global serialport_data
    global serialport_ind
    serialport_ind = ind;
    
    if field == 'fd' then
        res = serialport_data(ind).fd;
    elseif field == 'line_separator' then
        res = serialport_data(ind).line_separator;
    elseif or(field == ['baud', 'data_bits', 'stop_bits', 'parity', 'break', ...
                    'flow_control', 'dtr', 'dsr', 'rts', 'cts', 'dcd', 'ri', ...
                    'timeout']) then
        res = serialget(serialport_data(ind).fd, field);
    elseif field == 'read' then
        res = serialport_read;
    elseif field == 'readln' then
        res = serialport_readln;
    elseif field == 'write' then
        res = serialport_write;
    elseif field == 'writeln' then
        res = serialport_writeln;
    elseif field == 'close' then
        serialclose( serialport_data(ind).fd );
        serialport_data(ind) = null();
        res = serialport_null;
    else
        error( 'Field ''' + field + ''' not exists' );
    end
endfunction

function %serialpo_p(object)
    ind = serialport_find( object.name );
    if ind == 0 then
        mprintf( 'Closed serial port ' + object.name );
    else
        global serialport_data
        mprintf( 'Serial port ');
        for field = ['name', 'fd', 'buffer']
            mprintf('   %s: %s\n', field, ...
                    string(getfield(field, serialport_data(ind))));
        end
        mprintf('   line_separator: ' + ...
              strcat(string(ascii(serialport_data(ind).line_separator)), ' '));
        for field = ['baud', 'data_bits', 'stop_bits', 'parity', ...
                    'flow_control', 'dtr', 'dsr', 'rts', 'cts', 'dcd', 'ri', ...
                    'timeout']
            mprintf('   %s: %i\n', field, ...
                    serialget(serialport_data(ind).fd, field));
        end
    end
endfunction

