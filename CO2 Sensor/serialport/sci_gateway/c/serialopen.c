/* ==================================================================== */
/* Serial port - low level bindings */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "api_helper.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/ioctl.h>
/* ==================================================================== */
int serialopen(char *fname)
{
    int success = 1;
    
    /* check that we have 5 input arguments */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx,  5, 5);
    CheckOutputArgument(pvApiCtx, 1, 1);

    char *name = getString( 1, 4, 100, NULL, &success );
    if (!success) return 0;

    int   baud = getInt( 2, 0, 1000000, &success );
    if (!success) { free(name); return 0; };

    switch (baud)
    {
      case      50: baud =      B50; break;  
      case      75: baud =      B75; break;  
      case     110: baud =     B110; break;  
      case     134: baud =     B134; break;  
      case     150: baud =     B150; break;  
      case     200: baud =     B200; break;  
      case     300: baud =     B300; break;  
      case     600: baud =     B600; break;  
      case    1200: baud =    B1200; break;  
      case    2400: baud =    B2400; break;  
      case    4800: baud =    B4800; break;  
      case    9600: baud =    B9600; break;  
      case   19200: baud =   B19200; break;  
      case   38400: baud =   B38400; break;  
      case   57600: baud =   B57600; break;
#ifdef B76800
      case   76800: baud =   B76800; break;
#endif
#ifdef B115200
      case  115200: baud =  B115200; break;  
#endif
#ifdef B230400
      case  230400: baud =  B230400; break;  
#endif
#ifdef B460800
      case  460800: baud =  B460800; break;  
#endif
#ifdef B500000
      case  500000: baud =  B500000; break;  
#endif
#ifdef B576000
      case  576000: baud =  B576000; break;  
#endif
#ifdef B921600
      case  921600: baud =  B921600; break;  
#endif
#ifdef B1000000
      case 1000000: baud = B1000000; break;
#endif
      default:
        Scierror(999, "%s: Wrong value for input argument #%d:\n", fname, 2);
        success = 0;
    }

    char *parity = getString( 3, 1, 1, NULL, &success );
    if (!success) { free(name); return 0; };
    
    int data_bits = getInt( 4, 5, 8, &success );
    int stop_bits = getInt( 5, 1, 2, &success );
    if (!success) { free(name); free(parity); return 0; };
 
    switch (data_bits)
    {
      case 5: data_bits =  CS5; break;  
      case 6: data_bits =  CS6; break;  
      case 7: data_bits =  CS7; break;  
      case 8: data_bits =  CS8; break;
    }
    
    switch (stop_bits)
    {
      case 1: stop_bits =      0; break;  
      case 2: stop_bits = CSTOPB; break;  
    }

    //--------------------------------------------------------
    
    int fd = open(name, O_RDWR | O_NOCTTY | O_NDELAY);
    
    struct termios params;
     
    if (fd != -1) 
    {
      if (!isatty(fd))
      {
        close(fd);
        fd = -1;
      }
      else if (tcgetattr(fd, &params) == -1)
      {
        close(fd);
        fd = -1;
      }
      else        
      {
        fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) & ~O_NONBLOCK);
        params.c_cflag &= ~ICANON;
        params.c_oflag = 0;
        params.c_lflag = 0;
        params.c_iflag &= (IXON | IXOFF | IXANY);
        params.c_cflag |= CLOCAL | CREAD;
        params.c_cflag &= ~HUPCL;
        params.c_cflag &= ~CSIZE;
        params.c_cflag  |= data_bits;
        params.c_cflag &= ~CSTOPB;
        params.c_cflag  |= stop_bits;
        params.c_cc[VTIME] = 1;
        params.c_cc[VMIN] = 0;
        if (parity[0] == 'E' || parity[0] == 'e')
        {
          params.c_cflag |= PARENB;
          params.c_cflag &= ~PARODD;
        }
        else if (parity[0] == 'O' || parity[0] == 'o')
        {
          params.c_cflag |= PARENB;
          params.c_cflag |= PARODD;
        }
        else
        {
          params.c_cflag &= ~PARENB;
        }
        cfsetispeed(&params, baud);
        cfsetospeed(&params, baud);
        if (tcsetattr(fd, TCSANOW, &params) == -1)
        {
          close(fd);
          fd = -1;
        }
      }
    }
 
    //--------------------------------------------------------
    free(parity);
    free(name);

    /* create result on stack */
    returnInt( 1, fd );
    ReturnArguments(pvApiCtx);
    return 0;
}
/* ==================================================================== */

