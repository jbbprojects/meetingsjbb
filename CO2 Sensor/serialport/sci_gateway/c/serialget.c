/* ==================================================================== */
/* Serial port - low level bindings */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/ioctl.h>
#define  HARD 1
#define  SOFT 2
/* ==================================================================== */
int serialget(char *fname)
{
    int success = 1;
    struct termios params;
    int status;
    int ans;
    
    /* check that we have 2 input arguments */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx, 2, 2);
    CheckOutputArgument(pvApiCtx, 1, 1);
    
    int fd = getInt( 1, 0, 0x7FFFFFFF, &success );
    if (!success) return 0;

    if (!isatty(fd) || (tcgetattr(fd, &params) == -1))
    {
      Scierror(999, "It is not a serial port.\n");
      return 0;
    }   
    
    char *field = getString( 2, 1, 12, NULL, &success ); 
    if (!success) return 0;


    if (strcmp(field, "baud") == 0)
    {
      switch (cfgetospeed(&params))
      {
        case      B50: ans =      50; break;
        case      B75: ans =      75; break;  
        case     B110: ans =     110; break;  
        case     B134: ans =     134; break;  
        case     B150: ans =     150; break;  
        case     B200: ans =     200; break;  
        case     B300: ans =     300; break;  
        case     B600: ans =     600; break;  
        case    B1200: ans =    1200; break;  
        case    B2400: ans =    2400; break;  
        case    B4800: ans =    4800; break;  
        case    B9600: ans =    9600; break;  
        case   B19200: ans =   19200; break;  
        case   B38400: ans =   38400; break;  
        case   B57600: ans =   57600; break;
#ifdef B76800
        case   B76800: ans =   76800; break;
#endif
#ifdef B115200
        case  B115200: ans =  115200; break;  
#endif
#ifdef B230400
        case  B230400: ans =  230400; break;  
#endif
#ifdef B460800
        case  B460800: ans =  460800; break;  
#endif
#ifdef B500000
        case  B500000: ans =  500000; break;  
#endif
#ifdef B576000
        case  B576000: ans =  576000; break;  
#endif
#ifdef B921600
        case  B921600: ans =  921600; break;  
#endif
#ifdef B1000000
        case B1000000: ans = 1000000; break;
#endif
        default:
          ans = 0;
      }
    }
    else if (strcmp(field, "data_bits") == 0)
    {
      switch (params.c_cflag & CSIZE)
      {
        case CS5   : ans = 5; break;
        case CS6   : ans = 6; break;
        case CS7   : ans = 7; break;
        case CS8   : ans = 8; break;
        default:
          ans = 0;
      }
    }
    else if (strcmp(field, "stop_bits") == 0)
    {
      ans = (params.c_cflag & CSTOPB ? 2 : 1);
    }
    else if (strcmp(field, "parity") == 0)
    {
      if (!(params.c_cflag & PARENB))
        ans = 0;
      else if (params.c_cflag & PARODD)
        ans = 1;
      else
        ans = 2;
    }
    else if (strcmp(field, "break") == 0)
    {
      ans = 0;
    }
    else if (strcmp(field, "flow_control") == 0)
    {
      ans = 0;
      if (params.c_cflag & CRTSCTS) ans += HARD;
      if (params.c_iflag & (IXON | IXOFF | IXANY)) ans += SOFT;
    }
    else if (strcmp(field, "timeout") == 0)
    {
      if (params.c_cc[VTIME] == 0 && params.c_cc[VMIN] == 0)
        ans = -1;
      else
        ans = params.c_cc[VTIME] * 100;
    }
    else 
    {
      ioctl(fd, TIOCMGET, &status);
      if (strcmp(field, "dtr") == 0)
      {
        ans = (status & TIOCM_DTR ? 1 : 0);
      }
      else if (strcmp(field, "dsr") == 0)
      {
        ans = (status & TIOCM_DSR ? 1 : 0);
      }
      else if (strcmp(field, "rts") == 0)
      {
        ans = (status & TIOCM_RTS ? 1 : 0);
      }
      else if (strcmp(field, "cts") == 0)
      {
        ans = (status & TIOCM_CTS ? 1 : 0);
      }
      else if (strcmp(field, "dcd") == 0)
      {
        ans = (status & TIOCM_CD ? 1 : 0);
      }
      else if (strcmp(field, "ri") == 0)
      {
        ans = (status & TIOCM_RI ? 1 : 0);
      }
      else
      {
        Scierror(100, "%s: Unknown attribute %s\n", fname, field);
        ans = -1;
      }
    }
    
    //-----------------------------------------------------------------
    // Cleanup
    free(field);

    /* create result on stack */
    returnInt( 1, ans );
    ReturnArguments(pvApiCtx);
    return 0;
}
/* ==================================================================== */

