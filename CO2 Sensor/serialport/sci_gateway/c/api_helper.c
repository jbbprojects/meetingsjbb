/* ==================================================================== */
/* This module contains helpers for building of Scilab gateways.        */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
// 
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"

double getDouble( int index, double minValue, double maxValue, int *success)
{
    SciErr sciErr;
    int * address;
    double value;

    if (! *success) return 0.0;
    
    sciErr = getVarAddressFromPosition(pvApiCtx, index, &address);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	return 0.0;
    }
    
    if ( !isDoubleType(pvApiCtx, address) || !isScalar(pvApiCtx, address)) {
        Scierror(999, "Wrong type for input argument #%d: Scalar expected.\n", index);
	*success = 0;
        return 0.0;
    }
    
    sciErr.iErr = getScalarDouble(pvApiCtx, address, &value);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	return 0.0;
    }
    
    if (value < minValue || value > maxValue) {
        Scierror(999, "Input argument #%d is out of range %g..%g\n", index, minValue, maxValue);
        *success = 0;
    }
    
    return value;
}

int  getInt( int index, int minValue, int maxValue, int *success)
{
    return (int) getDouble( index, (double) minValue, (double) maxValue, success ); 
}   

void * getPtr (int index, int *success)
{
    SciErr sciErr;
    int * address;
    int inputType;
    void * result = NULL;

    if (! *success) return NULL;
    
    sciErr = getVarAddressFromPosition(pvApiCtx, index, &address);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
        *success = 0;
        return NULL;
    }
    
    sciErr = getVarType(pvApiCtx, address, &inputType);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
        *success = 0;
        return NULL;
    }
    
    if (inputType != sci_pointer) {
        Scierror(999, "Wrong type for input argument #%d: Pointer expected.\n", index);
        *success = 0;
        return NULL;
    }
    
    sciErr = getPointer(pvApiCtx, address, &result);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
        *success = 0;
        return NULL;
    }
    
    return result;
}

char * getString( int index, int minLength, int maxLength, int *len, int *success)
{
    SciErr sciErr;
    int   *address;
    double value;
    int    strLength;
    int    m, n;

    if (! *success) return NULL;
    
    sciErr = getVarAddressFromPosition(pvApiCtx, index, &address);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	return NULL;
    }
    
    if (!isStringType(pvApiCtx, address) || !isScalar(pvApiCtx, address)) {
        Scierror(999, "Wrong type for input argument #%d: Single string expected.\n", index);
	*success = 0;
        return NULL;
    }
    
    sciErr = getMatrixOfString(pvApiCtx, address, &m, &n, &strLength, NULL);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	return NULL;
    }
    
    if (strLength < minLength) {
        Scierror(999, "Input argument #%d: too short.\n", index);
	*success = 0;
        return NULL;
    }

    if (strLength > maxLength) {
        Scierror(999, "Input argument #%d: too long.\n", index);
	*success = 0;
        return NULL;
    }

    char *str = (char*)malloc(sizeof(char) * (strLength + 1)); 
    sciErr = getMatrixOfString(pvApiCtx, address, &m, &n, &strLength, &str); 
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	free(str);
	return NULL;
    }
    
    if (len != NULL) {
        *len = strLength;
    }
    
    return str;
}

int  getBoolean(int index, int* success) 
{
    SciErr sciErr;
    int  *address;
    int  answer;
    double value = 0.0;
    
    sciErr = getVarAddressFromPosition(pvApiCtx, index, &address);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	return 0;
    }

    int isBool = isBooleanType(pvApiCtx, address);
    
    if ( (!isBool && !isDoubleType(pvApiCtx, address)) || !isScalar(pvApiCtx, address)) {
        Scierror(999, "Wrong type for input argument #%d: Boolean or Double scalar expected.\n", index);
	*success = 0;
        return 0;
    }
    
    if (isBool) {
        sciErr.iErr = getScalarBoolean(pvApiCtx, address, &answer);
    } else {
        sciErr.iErr = getScalarDouble(pvApiCtx, address, &value);
	answer = (value == 0.0) ? 0 : 1;
    }
        
    if (sciErr.iErr) { 
	printError(&sciErr, 0);
	*success = 0;
	return 0;
    }
    
    return answer;
}


char * getBytes( int index, int minLength, int maxLength, int *len, int *freeIt, int *success)
{
    SciErr sciErr;
    int   *address;
    int    precision;
    int    strLength;
    int    m, n;
    char  *errorText = 
      "Wrong type for input argument #%d: Single string, int8 vector or uint8 vector are expected.\n";
    char  *bytes;
    
    *freeIt = 0;   
        
    if (! *success) return NULL;
        
    sciErr = getVarAddressFromPosition(pvApiCtx, index, &address);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
	*success = 0;
	return NULL;
    }
    
    if (isStringType(pvApiCtx, address)) {
        if (isScalar(pvApiCtx, address)) {
            *freeIt = 1;
            return getString(index, minLength, maxLength, len, success);
        }
    } else if (isIntegerType(pvApiCtx, address)) {
        
        sciErr = getMatrixOfIntegerPrecision(pvApiCtx, address, &precision);
        if (sciErr.iErr) {
            printError(&sciErr, 0);
            *success = 0;
            return NULL;
        }

        if (precision == SCI_INT8)
        {
            sciErr = getMatrixOfInteger8(pvApiCtx, address, &n, &m, &bytes);
        } 
        else if (precision == SCI_UINT8) 
        {
            sciErr = getMatrixOfUnsignedInteger8(pvApiCtx, address, &n, &m, &bytes);
        }
        else
        {
            Scierror(999, errorText, index);
            *success = 0;
            return NULL;
        }

        if(sciErr.iErr) {
            printError(&sciErr, 0);
            *success = 0;
            return NULL;
        }
        
        strLength = n * m;
        
        if (strLength < minLength) {
            Scierror(999, "Input argument #%d: too short.\n", index);
            *success = 0;
            return NULL;
        }

        if (strLength > maxLength) {
            Scierror(999, "Input argument #%d: too long.\n", index);
            *success = 0;
            return NULL;
        }

        if (len != NULL) {
            *len = strLength;
        }
    
        return bytes;
    } 

    Scierror(999, errorText, index);
    *success = 0;
    return NULL;
}


double* getDoubleVector (int index, int minLength, int maxLength, int* len, int* success)
{
    SciErr sciErr;
    int  * address;
    int    m, n;
    double * pdata;

    if (! *success) return NULL;
    
    sciErr = getVarAddressFromPosition(pvApiCtx, index, &address);
    if (sciErr.iErr) { 
        printError(&sciErr, 0);
        *success = 0;
        return NULL;
    }
    
    if ( !isDoubleType(pvApiCtx, address) ||  isVarComplex(pvApiCtx, address)) 
    {
        Scierror(999, "Wrong type for input argument #%d: Vector of Double (not complex) expected.\n", index);
        *success = 0;
        return NULL;
    }

    sciErr = getMatrixOfDouble(pvApiCtx, address, &n, &m, &pdata);

    if (n > 1 && m > 1)
    {
        Scierror(999, "Wrong type for input argument #%d: Vector expected.\n", index);
        *success = 0;
        return NULL;
    }

    if (n * m < minLength) {
        Scierror(999, "Input argument #%d: too short.\n", index);
        *success = 0;
        return NULL;
    }

    if (n * m > maxLength) {
        Scierror(999, "Input argument #%d: too long.\n", index);
        *success = 0;
        return NULL;
    }

    if (len != NULL) {
            *len = n * m;
    }
    
    return pdata;
}


void returnDouble( int index, double value) {
    double dOut = value;
    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + index, 1, 1, &dOut);
    AssignOutputVariable(pvApiCtx, index) = nbInputArgument(pvApiCtx) + index;
}

void returnInt(    int index, int value) {
    double dOut = (double)value;
    createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + index, 1, 1, &dOut);
    AssignOutputVariable(pvApiCtx, index) = nbInputArgument(pvApiCtx) + index;
}

void returnPtr(int index, void * value){
    createPointer(pvApiCtx, nbInputArgument(pvApiCtx) + index, value);
    AssignOutputVariable(pvApiCtx, index) = nbInputArgument(pvApiCtx) + index;
}

void returnString( int index, char *value){
    const char * strings[1] = {value};
    createMatrixOfString(pvApiCtx, nbInputArgument(pvApiCtx) + index, 1, 1, strings);
    AssignOutputVariable(pvApiCtx, index) = nbInputArgument(pvApiCtx) + index;
}

void returnBytes( int index, char *value, int len){
    createMatrixOfUnsignedInteger8(pvApiCtx, nbInputArgument(pvApiCtx) + index, 
				   1, len, value);
    AssignOutputVariable(pvApiCtx, index) = nbInputArgument(pvApiCtx) + index;
}

