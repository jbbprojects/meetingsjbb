#ifdef __cplusplus
extern "C" {
#endif
#include <mex.h> 
#include <sci_gateway.h>
#include <api_scilab.h>
#include <MALLOC.h>
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc serialopen;
extern Gatefunc serialclose;
extern Gatefunc serialread;
extern Gatefunc serialwrite;
extern Gatefunc serialget;
extern Gatefunc serialset;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway_without_putlhsvar,serialopen,"serialopen"},
  {(Myinterfun)sci_gateway_without_putlhsvar,serialclose,"serialclose"},
  {(Myinterfun)sci_gateway_without_putlhsvar,serialread,"serialread"},
  {(Myinterfun)sci_gateway_without_putlhsvar,serialwrite,"serialwrite"},
  {(Myinterfun)sci_gateway_without_putlhsvar,serialget,"serialget"},
  {(Myinterfun)sci_gateway_without_putlhsvar,serialset,"serialset"},
};
 
int C2F(libserialport)()
{
  Rhs = Max(0, Rhs);
  if (*(Tab[Fin-1].f) != NULL) 
  {
     if(pvApiCtx == NULL)
     {
       pvApiCtx = (StrCtx*)MALLOC(sizeof(StrCtx));
     }
     pvApiCtx->pstName = (char*)Tab[Fin-1].name;
    (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  }
  return 0;
}
#ifdef __cplusplus
}
#endif
