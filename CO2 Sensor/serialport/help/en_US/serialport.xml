<?xml version="1.0" encoding="UTF-8"?>
<!--
 * This file is released under the terms of the CeCILL_V2 license
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="serialport" xml:lang="en">
    <refnamediv>
        <refname>serialport</refname>
        <refpurpose>Create high level object (tlist) of class serialport</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>sp = serialport(name,parameters)</synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>name</term>
                <listitem>
                    <para>string, name of serial port</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>parameters</term>
                <listitem>
		  <para>varargin, strings or numbers, baud rate, parity, data_bits and stop bits. 
		    These values can be combined in specified order in 1 to 4 string or numeric scalar arguments. 
		    Default values are 9600,'N',8,1.</para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para>High level procedure.</para>
        <para>Open serial port and return the object.</para>
	<para>This object has following attributes and methods:</para>
    </refsection>
    <refsection>
        <title>Attributes</title>
        <variablelist>
            <varlistentry>
                <term>fd</term>
                <listitem>
		  <para>number, file handle.</para>
		</listitem>
            </varlistentry>
            <varlistentry>
                <term>baud</term>
                <listitem>
		  <para>number, baud rate</para>
		</listitem>
            </varlistentry> 
	    <varlistentry>
                <term>data_bits</term>
                <listitem>
		  <para>number, (5..8)</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>stop_bits</term>
                <listitem>
		  <para>number, 1 or 2</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>parity</term>
                <listitem>
		  <para>0 - none, 1 - Odd, 2 -Even</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>break</term>
                <listitem>
		  <para>number, 0 or 1</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>flow_control</term>
                <listitem>
		  <para>0 - none, 1 - Hard, 2 - Soft, 3 - Hard + Soft</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>dtr</term>
                <listitem>
		  <para>DTR status</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>dsr</term>
                <listitem>
		  <para>DSR status</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>rts</term>
                <listitem>
		  <para>RTS status</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>cts</term>
                <listitem>
		  <para>CTS status</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>dcd</term>
                <listitem>
		  <para>DCD status</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>ri</term>
                <listitem>
		  <para>RI status</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>timeout</term>
                <listitem>
		  <para>Timeout in milliseconds</para>
		</listitem>
            </varlistentry> 
            <varlistentry>
                <term>line_separator</term>
                <listitem>
                  <para>Characters used to mark end of line, default ascii(10)</para>
                </listitem>
            </varlistentry> 
        </variablelist>
        <title>Methods</title>
        <variablelist>
            <varlistentry>
                <term>write(str1[,str2,..])</term>
                <listitem>
                  <para>write string(s)</para>
                </listitem>
            </varlistentry> 
            <varlistentry>
                <term>writeln(str1[,str2,..])</term>
                <listitem>
                  <para>write string(s) and send line_separator after each row 
                    of each string</para>
                </listitem>
            </varlistentry> 
            <varlistentry>
                <term>s = read(count[,binary])</term>
                <listitem>
                  <para>Read and return string or bytes, default count is 1024.</para>
                </listitem>
            </varlistentry> 
            <varlistentry>
                <term>s = readln()</term>
                <listitem>
                  <para>Read up to line_separator, return string</para>
                </listitem>
            </varlistentry> 
            <varlistentry>
                <term>close()</term>
                <listitem>
                  <para>close serial port</para>
                </listitem>
            </varlistentry> 
        </variablelist>
      </refsection> 
      <refsection>
        <title>Examples</title>
	<programlisting role="example">
	    sp = serialport('/dev/ttyS0', '19200,N, 8, 1')
	    disp( sp.ri )
	    sp.writeln('*idn?')
	    answer = sp.readln()  # NOTE! Brackets required.
	    mprintf( answer )
	    sp.close()
	</programlisting>
      </refsection>
      <refsection role="see also">
        <title>See Also. Low level functions:</title>
        <simplelist type="inline">
            <member>
                <link linkend="serialopen">serialopen</link>
            </member>
            <member>
                <link linkend="serialclose">serialclose</link>
            </member>
            <member>
                <link linkend="serialread">serialread</link>
            </member>
            <member>
                <link linkend="serialwrite">serialwrite</link>
            </member>
            <member>
                <link linkend="serialget">serialget</link>
            </member>
            <member>
                <link linkend="serialset">serialset</link>
            </member>
        </simplelist>
    </refsection>
    <refsection>
        <title>Author</title>
        <simplelist type="vert">
            <member>Oleg BELOV</member>
        </simplelist>
    </refsection>
</refentry>
