# README #

This README would normally document whatever steps are necessary to get your application up and running.

First: check our library results in [zotero](https://www.zotero.org/groups/seederjbbproject)

You need:

* MAX6675 Cold-Junction-Compensated K-Thermocouple-to-digital Converter to an Arduino micro-controller. A Type K thermal couple is inexpensive and fairly accurate. Here we have a temperature range of 0 deg C to 1024 deg C.

The MAX6675 performs cold-junction compensation and digitalizes the signal form a type-K thermocouple. The data is outpur sin a 12-bit resolution, SPI-compatible, read-only format.

Might you check this [link](http://www.bristolwatch.com/ele2/therc.htm)
### What is this repository for? ###

* Quick summary

* ¿Qué se quiere hacer?: 
Sistema de progpagación de especies nativas por esqueje, con sistema de control de variables como la temperatura y la humedad inicialmente.

* Objetivos
>1.	Evaluar las varibles de mayor impacto en el crecimiento de las plantas.
>2.	Garantizar condiciones estables en las varibles críticas en el proceso de propagación de plantas nativas (escojer especies).

* Requerimientos funcionales

1. 	Medición de variables

Variable          | Sensor   | Costo 
-----------------:| :------- |:-----:
[Temperatura ](http://www.electrodragon.com/w/index.php?title=AM2302&redirect=no)      | DHT22    | 
[pH](http://gardenbot.org/howTo/soilMoisture/)                |  [SEN0161](https://www.dfrobot.com/wiki/index.php/PH_meter(SKU:_SEN0161))  | U$ 29
Humedad           | DHT22    |
Radiación solar   | BH1750   | 
CO_2              | MHZ16    | 

2.  	Control de variables

Variable          | Actuador  | Costo 
-----------------:| :-------- |:-----:
Temperatura       |           | 
Riego             |           | 
Humedad           |           |
Radiación solar   |           | 

3.  	Se requiere de una unidad de procesamiento que permita:

- Para que envie las señales de control a los actuadores en función de la información que provean los sensores.

- Remitir la información obtenida de los sensores a una plataforma (local o web) en la que se puedan procesar.

- Mostrar curvas temporales del reporte de los sensores.
- Evaluar la posibilidad de controlar el sistema desde celular o computador, en especial para la función de riego.

* Requerimientos no funcionales

>1.  	Resistencia a la exposición a ambientes húmedos y lluviosos.

>2.  	Resistencia a medio ácido-básico.

>3.  	Agente documental del crecimiento de las plantas.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: 3-wire connection between the MAX6675 Cold-Juntion-Compensated K-Thermocouple-to-Digital Converter and Arduino. This is a read only device with a 12-bit output (16 total bits)mwith bit 15 (MSB) output first.
>1.	Bit 15 is a dummy sign and is discarded.
>2.	Bits 14-3 are actual temperature reading.
>3.	Bit 2 is normally 0 but if sensor is attached
>4.	Bits 0 and 1 are discarded.


* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Please if you are interested, feel free contact [Andres Duque](afduquem@unal.edu.co) and [Mayerli Montes](mamontesp@unal.edu.co)
* Other community or team contact