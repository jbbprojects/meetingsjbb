// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_c()

  includes_src_c = ilib_include_flag(get_absolute_file_path("builder_gateway_c.sce"));

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %t;

  tbx_build_gateway("serialport", ..
                    ["serialopen",  "serialopen"; ...
                     "serialclose", "serialclose"; ...
                     "serialread",  "serialread"; ...
                     "serialwrite", "serialwrite"; ...
                     "serialget",   "serialget"; ...
                     "serialset",   "serialset"], ..
                    ["serialopen.c", "serialclose.c", ...
                     "serialread.c", "serialwrite.c", ...
                     "serialget.c",  "serialset.c", "api_helper.c"], ..
                    get_absolute_file_path("builder_gateway_c.sce"), ..
                    [], ..
                    "", ..
                   includes_src_c); // + " -save-temps -fverbose-asm ");
                   
endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
