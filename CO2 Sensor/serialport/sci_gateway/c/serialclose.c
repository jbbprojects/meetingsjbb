/* ==================================================================== */
/* Serial port - low level bindings */
/* This file is released under the terms of the CeCILL_V2 license
/* ==================================================================== */
#include "api_helper.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "api_helper.h"
#include "MALLOC.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/ioctl.h>
/* ==================================================================== */
int serialclose(char *fname)
{
    int success = 1;
    
    /* check that we have only 1 input arguments */
    /* check that we have only 1 output argument */
    CheckInputArgument(pvApiCtx, 1, 1);
    CheckOutputArgument(pvApiCtx, 1, 1);
    
    int fd = getInt( 1, 0, 0x7FFFFFFF, &success );
    
    if (!success) return 0;
    
    returnInt( 1, close(fd));
    ReturnArguments(pvApiCtx);
    return 0;
}
/* ==================================================================== */

